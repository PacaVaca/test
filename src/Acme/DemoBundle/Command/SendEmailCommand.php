<?php

namespace Acme\DemoBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendEmailCommand extends ContainerAwareCommand
{
    /**
     * @var $container \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     * @var $em \Doctrine\ORM\EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('send:email')
            ->setDescription('Sending emails');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $emailManager = $this->container->get('manager.email');
        $count = $emailManager->sendEmailsForUsers();
        $output->write('successful: ' . $count);
    }
}