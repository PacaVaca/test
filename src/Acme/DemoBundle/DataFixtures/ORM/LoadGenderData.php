<?php
namespace Acme\DemoBundle\DataFixtures\ORM;

use Acme\DemoBundle\Entity\Gender;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadGenderData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $genderMale = new Gender();
        $genderMale->setGender(Gender::GENDER_MALE);
        $manager->persist($genderMale);

        $genderFemale = new Gender();
        $genderFemale->setGender(Gender::GENDER_MALE);
        $manager->persist($genderFemale);

        $manager->flush();

        $this->addReference(Gender::GENDER_MALE, $genderMale);
        $this->addReference(Gender::GENDER_FEMALE, $genderFemale);
    }

    public function getOrder()
    {
        return 1;
    }
}