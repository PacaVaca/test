<?php
namespace Acme\DemoBundle\DataFixtures\ORM;

use Acme\DemoBundle\Entity\Gender;
use Acme\DemoBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            if (rand(0, 1)) {
                $user->setGender($this->getReference(Gender::GENDER_MALE));
            } else {
                $user->setGender($this->getReference(Gender::GENDER_FEMALE));
            }
            $user->setName('admin');
            $user->setEmail('email@email.com');
            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}