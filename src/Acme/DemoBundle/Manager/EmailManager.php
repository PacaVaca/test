<?php
namespace Acme\DemoBundle\Manager;

use Acme\DemoBundle\Entity\Gender;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Twig\TwigEngine;

class EmailManager
{
    private $om;
    private $mailer;
    private $templating;
    private $fromEmail;

    public function __construct(ObjectManager $om, \Swift_Mailer $mailer, TwigEngine $templating, $fromEmail)
    {
        $this->om = $om;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @return \Acme\DemoBundle\Repository\UserRepository
     */
    private function getUserRepository()
    {
        return $this->om->getRepository("AcmeDemoBundle:User");
    }

    public function sendEmailsForUsers()
    {
        $users = $this->getUserRepository()->getByBirthday();
        $sentCount = 0;
        foreach ($users as $user) {
            try {
                $template = $this->templating
                    ->render('AcmeDemoBundle:Email:user_birthday.html.twig', ['user' => $user]);
                $message = new \Swift_Message();
                if ($user->getGender()->getGender() === Gender::GENDER_MALE) {
                    $attachment = 'male.jpg';
                } else {
                    $attachment = 'female.jpg';
                }
                $to = 'test@domain.com'; // In real world should be retreaved from user or somewhere else
                $message->addTo($to)
                    ->addFrom($this->fromEmail)
                    ->setSubject('testing')
                    ->setBody($template, 'text/html')
                    ->attach(\Swift_Attachment::fromPath(realpath('./web/images/' . $attachment)));

                $this->mailer->send($message);
                $sentCount++;
            } catch (\Exception $e) {

            }
        }

        return $sentCount;
    }
}