<?php
namespace Acme\DemoBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getByBirthday()
    {
        $today = (new \DateTime())->setTime(0, 0, 0);
        $tomorrow = clone $today;
        $qb = $this->createQueryBuilder('u')
            ->select('u, g')
            ->leftJoin('u.gender', 'g')
            ->where('u.birthday >= :begin')
            ->andWhere('u.birthday < :end')
            ->setParameter('begin', $today)
            ->setParameter('end', $tomorrow->modify('next day'));

        return $qb->getQuery()->getResult();
    }
}